const { src, dest, task } = require("gulp");
const replace = require("gulp-replace");

task("rename:scss", () => {
  return src("dist/**/*.js", { base: "." })
    .pipe(replace(/.scss/g, ".css"))
    .pipe(dest("."));
});
