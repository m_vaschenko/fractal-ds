const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// Export a function. Accept the base config as the only param.
module.exports = {
  stories: ["../**/*.stories.[tj]s"],

  webpackFinal: async (config, { configType }) => {
    // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    // You can change the configuration based on that.
    // 'PRODUCTION' is used when building the static version of storybook.

    // Make whatever fine-grained changes you need
    config.module.rules.push({
      test: /\.scss$/,
      use: ["style-loader", "css-loader", "sass-loader"],
      include: path.resolve(__dirname, "../src")
    });

    config.module.rules.push({
      test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
      exclude: /node_modules/,
      loader: 'url-loader',
      options: {
        limit: 10000,
        name: '[hash]-[name].[ext]',
      },
    });

    config.plugins.push(
      new MiniCssExtractPlugin({ filename: "[name].module.css" })
    );

    // Return the altered config
    return config;
  }
};
