// src/components/Task.js

import React from "react";
import "./Hint.scss";
import PropTypes from "prop-types";

export default function Hint({ data: { hintId, hintText }, onClickHandler }) {
  return (
    <p
      id={`odessa-hint-${hintId}`}
      className="odessa-hint"
      onClick={onClickHandler}
    >
      {hintText}
    </p>
  );
}

Hint.propTypes = {
  data: PropTypes.shape({
    hintId: PropTypes.string.isRequired,
    hintText: PropTypes.string.isRequired
  })
};
