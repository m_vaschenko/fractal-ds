import React from "react";
import "./Input.scss";

export default function Input({
  data: { id, char, hintId, value, name },
  onChange,
  onBlur
}) {
  return (
    <input
      className={`odessa-input${char && "--" + char + "-char-width"}`}
      aria-describedby={hintId && `odessa-hint-${hintId}`}
      type="text"
      id={id}
      onBlur={onBlur}
      onChange={onChange}
      value={value}
      name={name}
    />
  );
}
