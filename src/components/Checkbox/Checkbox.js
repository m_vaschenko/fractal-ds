import React from "react";

export default function Input({ data: { id, name, value }, onChange, onBlur }) {
  return (
    <div className="odessa-checkboxes">
      <div className="odessa-checkboxes__item">
        <label htmlFor={id} className="odessa-checkboxes__label">
          <input
            className="odessa-checkboxes__input"
            type="checkbox"
            value={value}
            name={name}
            id={id}
            onBlur={e => onBlur(e)}
            onChange={e => onChange(e)}
          />
        </label>
      </div>
    </div>
  );
}

// <input class="odessa-checkboxes__input" id="{{this.checkboxId}}" name="{{this.checkboxName}}" type="checkbox" value="{{this.checkboxValue}}">
