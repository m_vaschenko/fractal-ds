// src/components/Task.js
import React from "react";
import "./Button.scss";

export default function Button({
  data: { id, text, skin },
  onClickHandler,
  disabled
}) {
  return (
    <button
      id={id}
      className={`odessa-button--${skin}`}
      onClick={onClickHandler}
      disabled={disabled}
    >
      {text}
    </button>
  );
}
