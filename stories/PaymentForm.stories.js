import React from "react";
import PaymentForm from "../src/components/PaymentForm/PaymentForm";
import withFormik from "storybook-formik";

export default {
  decorators: [withFormik],
  component: PaymentForm,
  title: "Examples",
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/
};

export const PaymentFormExample = () => {
  return (
    <div className="exampleWrapper">
      <PaymentForm />
    </div>
  );
};
