import React from "react";
import Input from "../Input/Input";
import Label from "../Label/Label";
import Hint from "../Hint/Hint";
import { idGenerator } from "../../utils/idGenerator";
import "./TextInput.css";
var defaultId = idGenerator();
export default function TextInput(_ref) {
  var _ref$data = _ref.data,
      _ref$data$id = _ref$data.id,
      id = _ref$data$id === void 0 ? defaultId : _ref$data$id,
      labelText = _ref$data.labelText,
      labelType = _ref$data.labelType,
      labelFlag = _ref$data.labelFlag,
      inputChar = _ref$data.inputChar,
      hintText = _ref$data.hintText,
      hintId = _ref$data.hintId,
      error = _ref$data.error,
      value = _ref$data.value,
      name = _ref$data.name,
      onChange = _ref.onChange,
      onBlur = _ref.onBlur;
  return React.createElement("div", {
    className: "odessa-form-group ".concat(error ? "odessa-TextInput-error" : "")
  }, React.createElement(Label, {
    data: {
      id: id,
      text: labelText,
      type: labelType,
      flag: labelFlag
    }
  }), hintText && React.createElement(Hint, {
    data: {
      hintId: hintId,
      hintText: hintText
    }
  }), error && React.createElement("div", {
    className: "odessa-TextInput-errorText"
  }, error), React.createElement(Input, {
    data: {
      "char": inputChar,
      id: id,
      hintId: hintId,
      value: value,
      name: name
    },
    onChange: onChange,
    onBlur: onBlur
  }));
}