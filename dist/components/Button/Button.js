// src/components/Task.js
import React from "react";
import "./Button.css";
export default function Button(_ref) {
  var _ref$data = _ref.data,
      id = _ref$data.id,
      text = _ref$data.text,
      skin = _ref$data.skin,
      onClickHandler = _ref.onClickHandler,
      disabled = _ref.disabled;
  return React.createElement("button", {
    id: id,
    className: "odessa-button--".concat(skin),
    onClick: onClickHandler,
    disabled: disabled
  }, text);
}