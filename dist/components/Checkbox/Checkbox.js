import React from "react";
export default function Input(_ref) {
  var _ref$data = _ref.data,
      id = _ref$data.id,
      name = _ref$data.name,
      value = _ref$data.value,
      _onChange = _ref.onChange,
      _onBlur = _ref.onBlur;
  return React.createElement("div", {
    className: "odessa-checkboxes"
  }, React.createElement("div", {
    className: "odessa-checkboxes__item"
  }, React.createElement("label", {
    htmlFor: id,
    className: "odessa-checkboxes__label"
  }, React.createElement("input", {
    className: "odessa-checkboxes__input",
    type: "checkbox",
    value: value,
    name: name,
    id: id,
    onBlur: function onBlur(e) {
      return _onBlur(e);
    },
    onChange: function onChange(e) {
      return _onChange(e);
    }
  }))));
} // <input class="odessa-checkboxes__input" id="{{this.checkboxId}}" name="{{this.checkboxName}}" type="checkbox" value="{{this.checkboxValue}}">