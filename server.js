const express = require("express");
const app = express();

app.use(express.static("storybook-static"));

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/storybook-static/index.html");
});

app.listen(3001, () =>
  console.log("Server app listening on port http://localhost:3001")
);
